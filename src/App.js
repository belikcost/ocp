import { SelectableOption } from "./components/SelectableOption";
import { SearchContainer } from "./components/Search";


const App = () => {

    const headerSelectableOption = {
        name: "Suppliers",
        values: [
            "Lenta Warehouse",
            "Hashicorp",
            "Apple inc."
        ],
        value: ''
    };

    const footerSelectableOption = {
        name: "Customers",
        values: [
            "Lenta Warehouse",
            "Hashicorp",
            "Apple inc."
        ],
        value: ''
    };

    return (
        <div>
            <header>
                <SearchContainer>
                    <SelectableOption data={headerSelectableOption}/>
                    <SelectableOption data={footerSelectableOption}/>
                </SearchContainer>
            </header>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt dolorum, eligendi maxime qui sit
                suscipit unde. Alias aliquid, cupiditate eos et excepturi hic inventore minima nam nobis numquam
                perspiciatis quaerat quas quia quisquam reiciendis rem repellendus saepe sint suscipit totam ullam vero
                vitae voluptatum? Corporis, distinctio dolorum est illo incidunt nisi porro praesentium quaerat
                similique veritatis!
            </p>
            <footer>
                <SearchContainer>
                    <SelectableOption data={footerSelectableOption}/>
                </SearchContainer>
            </footer>
        </div>
    )
};

export default App;
