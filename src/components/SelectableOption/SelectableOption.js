export const SelectableOption = ({data, handleSetData}) => {

    return (
        <label>
            <p>{data.name}</p>
            <select
                defaultValue="default"
                onChange={(e) => handleSetData({...data, value: e.target.value})}
            >
                <option value="default" disabled>Выберите значение</option>
                {data.values.map((selectableValue, i) => (
                    <option value={selectableValue} key={i}>{selectableValue}</option>
                ))}
            </select>
        </label>
    );
};