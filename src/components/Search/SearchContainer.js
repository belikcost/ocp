import React, { useEffect, useState } from 'react';

import { Search } from "./Search";


export const SearchContainer = ({children}) => {
    const [searchString, setSearchString] = useState('');
    const [data, setData] = useState([]);

    const handleSendRequest = () => {
        console.log(`Send request!`, data, searchString);
    }

    const handleSearchStringChange = (e) => {
        setSearchString(e.target.value);
    };

    const isArray = (value) => Array.isArray(value);

    useEffect(() => {
        if (children) {
            const childrenData = [];

            if (isArray(children)) {
                children.forEach(child => {
                    childrenData.push(child.props.data);
                });
            } else {
                childrenData.push(children.props.data);
            }

            setData(childrenData);
        }
    }, [children]);

    const createChildrenSetData = (children) => (newChildrenData) => setData(data.map(childrenData =>
        childrenData.name === children.props.data.name ? (newChildrenData) : (childrenData)));

    const createChildrenWithProps = (children) => ({
        ...children,
        props: {
            ...children.props,
            handleSetData: createChildrenSetData(children)
        }
    });

    return (
        <Search value={searchString} handleChange={handleSearchStringChange} handleSendRequest={handleSendRequest}>
            {children && (isArray(children) ?
                children.map(child => createChildrenWithProps(child)) : createChildrenWithProps(children))
            }
        </Search>
    );
}