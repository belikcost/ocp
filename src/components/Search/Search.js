export const Search = ({searchString, handleChange, handleSendRequest, children}) => {

    return (
      <div>
          <input type="text" value={searchString} onChange={handleChange}/>
          <button onClick={handleSendRequest}>Search</button>
          <div>
              {children}
          </div>
      </div>
    );
}